let w:unknown = true;
w = "string";
w = {
    runANonExistenMethod: () => {
        console.log("I Think therefore I am")
    }
} as { runANonExistenMethod: () => void }

if(typeof w === "object" && w!==null){
    (w as { runANonExistenMethod: Function }).runANonExistenMethod();
}
